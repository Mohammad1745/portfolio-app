<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHeaderMenu extends Model
{
    protected $fillable = ['user_id', 'header_menu_id'];
}
