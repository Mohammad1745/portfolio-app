<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    protected $fillable = ['user_id', 'job_position', 'company', 'url', 'responsibility', 'started_at', 'ended_at'];
}
