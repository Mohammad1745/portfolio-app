<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['user_id', 'title', 'subtitle', 'url', 'amount', 'content', 'status'];
}
