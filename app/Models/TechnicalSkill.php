<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TechnicalSkill extends Model
{
    protected $fillable = ['user_id', 'title', 'percentage'];
}
