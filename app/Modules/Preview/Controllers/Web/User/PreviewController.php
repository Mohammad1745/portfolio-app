<?php

namespace App\Modules\Preview\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\Preview\Services\PreviewService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PreviewController extends Controller
{
    private $previewService;

    /**
     * PreviewController constructor.
     * @param PreviewService $previewService
     */
    public function __construct(PreviewService $previewService)
    {
        $this->previewService = $previewService;
    }

    /**
     * @return Application|Factory|View
     */
    public function preview()
    {
        $data['mainMenu'] = 'preview';
        $data['user'] = Auth::user();
        $data['profession'] = $this->previewService->userProfession();
        $data['technicalSkills'] = $this->previewService->userTechnicalSkills();
        $data['professionalSkills'] = $this->previewService->userProfessionalSkills();
        $data['about'] = $this->previewService->userAbout();
        $data['activities'] = $this->previewService->userActivities();
        $data['featuredProjects'] = $this->previewService->userFeaturedProjects();
        $data['educations'] = $this->previewService->educations();
        $data['workExperiences'] = $this->previewService->workExperiences();
        $data['portfolioCategories'] = $this->previewService->portfolioCategories();
        $data['portfolioContents'] = $this->previewService->portfolioContents();
        $data['prices'] = $this->previewService->prices();
        $data['posts'] = $this->previewService->posts();

        return view('user.preview.content', $data);
    }
}
