<?php


namespace App\Modules\Preview\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\PortfolioContent;

class PortfolioContentRepository extends CommonRepository
{
    /**
     * PortfolioContentRepository constructor.
     */
    public function __construct()
    {
        $model = new PortfolioContent();
        parent::__construct($model);
    }
}
