<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('preview', 'PreviewController@preview')->name('preview');
});

