<?php

namespace App\Modules\Settings\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\Settings\Services\SettingsService;
use Illuminate\Http\RedirectResponse;

class SettingsController extends Controller
{
    private $settingsService;

    /**
     * SettingsController constructor.
     * @param SettingsService $settingsService
     */
    public function __construct(SettingsService $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * @param $slug
     * @param $value
     * @return RedirectResponse
     */
    public function updateSettings($slug, $value)
    {
        $response = $this->settingsService->updateSettings($slug, $value);

        return redirect()->back()->with($response['webResponse']);
    }
}
