<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('update-settings/{slug}={value}', "SettingsController@updateSettings")->name('updateSettings');
});

