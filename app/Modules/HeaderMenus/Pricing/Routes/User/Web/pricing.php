<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('pricing', "PricingController@pricing")->name('headerMenu.pricing');
    Route::post('add-price', "PricingController@storePrice")->name('headerMenu.storePrice');
    Route::post('edit-price', "PricingController@updatePrice")->name('headerMenu.updatePrice');
    Route::get('delete-price/{encryptedId}', "PricingController@deletePrice")->name('headerMenu.deletePrice');
    Route::get('activate-price/{encryptedId}', "PricingController@activatePrice")->name('headerMenu.activatePrice');
    Route::get('deactivate-price/{encryptedId}', "PricingController@deactivatePrice")->name('headerMenu.deactivatePrice');
});

