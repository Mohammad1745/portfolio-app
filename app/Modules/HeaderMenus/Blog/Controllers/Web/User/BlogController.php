<?php

namespace App\Modules\HeaderMenus\Blog\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Blog\Requests\StorePostRequest;
use App\Modules\HeaderMenus\Blog\Requests\UpdatePostRequest;
use App\Modules\HeaderMenus\Blog\Services\BlogService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BlogController extends Controller
{
    private $blogService;

    /**
     * BlogController constructor.
     * @param BlogService $blogService
     */
    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }

    /**
     * @return Application|Factory|View
     */
    public function blog()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'blog';
        $data['user'] = Auth::user();
        $data['posts'] = $this->blogService->posts();

        return view('user.header_menus.blog', $data);
    }

    /**
     * @param StorePostRequest $request
     * @return RedirectResponse
     */
    public function storePost(StorePostRequest $request)
    {
        $response = $this->blogService->storePost($request);

        return $response['success'] ?
            redirect()->route('headerMenu.blog')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdatePostRequest $request
     * @return RedirectResponse
     */
    public function updatePost(UpdatePostRequest $request)
    {
        $response = $this->blogService->updatePost($request);

        return $response['success'] ?
            redirect()->route('headerMenu.blog')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deletePost($encryptedId)
    {
        $response = $this->blogService->deletePost($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
