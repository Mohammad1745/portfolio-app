<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('blog', "BlogController@blog")->name('headerMenu.blog');
    Route::post('add-post', "BlogController@storePost")->name('headerMenu.storePost');
    Route::post('edit-post', "BlogController@updatePost")->name('headerMenu.updatePost');
    Route::get('delete-post/{encryptedId}', "BlogController@deletePost")->name('headerMenu.deletePost');
});

