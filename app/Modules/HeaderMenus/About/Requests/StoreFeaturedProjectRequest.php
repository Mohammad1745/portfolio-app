<?php

namespace App\Modules\HeaderMenus\About\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFeaturedProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_type' => 'required|string',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,JPG,png,PNG,gif|max:4000',
            'url' => isset($this->url) ? 'required|string' : '',
        ];
    }
}
