<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('about', "AboutController@about")->name('headerMenu.about');
    Route::post('edit-description', "AboutController@updateDescription")->name('headerMenu.updateDescription');
    Route::post('add-activity', "AboutController@storeActivity")->name('headerMenu.storeActivity');
    Route::post('edit-activity', "AboutController@updateActivity")->name('headerMenu.updateActivity');
    Route::get('delete-activity/{encryptedId}', "AboutController@deleteActivity")->name('headerMenu.deleteActivity');
    Route::post('add-featured-project', "AboutController@storeFeaturedProject")->name('headerMenu.storeFeaturedProject');
    Route::post('edit-featured-project', "AboutController@updateFeaturedProject")->name('headerMenu.updateFeaturedProject');
    Route::get('delete-featured-project/{encryptedId}', "AboutController@deleteFeaturedProject")->name('headerMenu.deleteFeaturedProject');
    Route::post('add-project-comment', "AboutController@storeProjectComment")->name('headerMenu.storeProjectComment');
    Route::post('edit-project-comment', "AboutController@updateProjectComment")->name('headerMenu.updateProjectComment');
    Route::get('delete-project-comment/{encryptedId}', "AboutController@deleteProjectComment")->name('headerMenu.deleteProjectComment');
});

