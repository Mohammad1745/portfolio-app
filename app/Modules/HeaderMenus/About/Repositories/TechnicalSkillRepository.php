<?php


namespace App\Modules\HeaderMenus\About\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\TechnicalSkill;

class TechnicalSkillRepository extends CommonRepository
{
    /**
     * TechnicalSkillRepository constructor.
     */
    public function __construct()
    {
        $model = new TechnicalSkill();
        parent::__construct($model);
    }
}
