<?php

namespace App\Modules\HeaderMenus\About\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\About\Requests\StoreActivityRequest;
use App\Modules\HeaderMenus\About\Requests\StoreFeaturedProjectRequest;
use App\Modules\HeaderMenus\About\Requests\StoreProjectCommentRequest;
use App\Modules\HeaderMenus\About\Requests\UpdateActivityRequest;
use App\Modules\HeaderMenus\About\Requests\UpdateDescriptionRequest;
use App\Modules\HeaderMenus\About\Requests\UpdateFeaturedProjectRequest;
use App\Modules\HeaderMenus\About\Requests\UpdateProjectCommentRequest;
use App\Modules\HeaderMenus\About\Services\AboutService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AboutController extends Controller
{
    private $aboutService;

    /**
     * AboutController constructor.
     * @param AboutService $aboutService
     */
    public function __construct(AboutService $aboutService)
    {
        $this->aboutService = $aboutService;
    }

    /**
     * @return Application|Factory|View
     */
    public function about()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'about';
        $data['user'] = Auth::user();
        $data['about'] = $this->aboutService->userAbout();
        $data['technicalSkills'] = $this->aboutService->userTechnicalSkills();
        $data['activities'] = $this->aboutService->userActivities();
        $data['featuredProjects'] = $this->aboutService->userFeaturedProjects();

        return view('user.header_menus.about', $data);
    }

    /**
     * @param UpdateDescriptionRequest $request
     * @return RedirectResponse
     */
    public function updateDescription(UpdateDescriptionRequest $request)
    {
        $response = $this->aboutService->updateDescription($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StoreActivityRequest $request
     * @return RedirectResponse
     */
    public function storeActivity(StoreActivityRequest $request)
    {
        $response = $this->aboutService->storeActivity($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdateActivityRequest $request
     * @return RedirectResponse
     */
    public function updateActivity(UpdateActivityRequest $request)
    {
        $response = $this->aboutService->updateActivity($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteActivity($encryptedId)
    {
        $response = $this->aboutService->deleteActivity($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StoreFeaturedProjectRequest $request
     * @return RedirectResponse
     */
    public function storeFeaturedProject(StoreFeaturedProjectRequest $request)
    {
        $response = $this->aboutService->storeFeaturedProject($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }
    public function updateFeaturedProject(UpdateFeaturedProjectRequest $request)
    {
        $response = $this->aboutService->updateFeaturedProject($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteFeaturedProject($encryptedId)
    {
        $response = $this->aboutService->deleteFeaturedProject($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StoreProjectCommentRequest $request
     * @return RedirectResponse
     */
    public function storeProjectComment(StoreProjectCommentRequest $request)
    {
        $response = $this->aboutService->storeProjectComment($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }
    public function updateProjectComment(UpdateProjectCommentRequest $request)
    {
        $response = $this->aboutService->updateProjectComment($request);

        return $response['success'] ?
            redirect()->route('headerMenu.about')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteProjectComment($encryptedId)
    {
        $response = $this->aboutService->deleteProjectComment($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
