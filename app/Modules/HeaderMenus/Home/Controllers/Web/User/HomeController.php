<?php

namespace App\Modules\HeaderMenus\Home\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Home\Requests\UpdateProfessionRequest;
use App\Modules\HeaderMenus\Home\Services\HomeService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HomeController extends Controller
{
    private $homeService;

    /**
     * HomeController constructor.
     * @param HomeService $homeService
     */
    public function __construct(HomeService $homeService)
    {
        $this->homeService = $homeService;
    }

    /**
     * @return Application|Factory|View
     */
    public function home()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'home';
        $data['user'] = Auth::user();
        $data['profession'] = $this->homeService->userProfession();

        return view('user.header_menus.home', $data);
    }

    /**
     * @param UpdateProfessionRequest $request
     * @return RedirectResponse
     */
    public function updateProfession(UpdateProfessionRequest $request)
    {
        $response = $this->homeService->updateProfession($request);

        return $response['success'] ?
            redirect()->route('headerMenu.home')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }
}
