<?php


namespace App\Modules\HeaderMenus\Home\Services;

use App\Modules\HeaderMenus\Home\Repositories\UserProfessionRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class HomeService
{
    private $errorMessage;
    private $errorResponse;
    private $userProfessionRepository;

    /**
     * HomeService constructor.
     * @param UserProfessionRepository $userProfessionRepository
     */
    public function __construct(UserProfessionRepository $userProfessionRepository)
    {
        $this->userProfessionRepository = $userProfessionRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function userProfession()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->userProfessionRepository->whereLast($where);
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProfession($request)
    {
        try{
            $where = ['user_id' => Auth::user()->id];
            $professionData = ['title' => $request->profession];
            $this->userProfessionRepository->updateOrCreate($where,$professionData);

            return [
                'success' => true,
                'message' => 'Profession has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Profession has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
