<?php


namespace App\Modules\HeaderMenus\Contact\Repositories;

use App\Http\Repositories\CommonRepository;
use App\User;

class UserRepository extends CommonRepository
{
    /**
     * HeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new User();
        parent::__construct($model);
    }
}
