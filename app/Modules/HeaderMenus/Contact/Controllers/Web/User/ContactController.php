<?php

namespace App\Modules\HeaderMenus\Contact\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Contact\Services\ContactService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ContactController extends Controller
{
    private $contactService;

    /**
     * ContactController constructor.
     * @param ContactService $contactService
     */
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * @return Application|Factory|View
     */
    public function contact()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'contact';
        $data['user'] = Auth::user();

        return view('user.header_menus.contact', $data);
    }
}
