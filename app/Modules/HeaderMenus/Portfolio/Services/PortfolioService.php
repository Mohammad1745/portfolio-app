<?php


namespace App\Modules\HeaderMenus\Portfolio\Services;

use App\Modules\HeaderMenus\Portfolio\Repositories\PortfolioContentRepository;
use App\Modules\HeaderMenus\Portfolio\Repositories\PortfolioCategoryRepository;
use Illuminate\Support\Facades\Auth;

class PortfolioService
{
    private $errorMessage;
    private $errorResponse;
    private $portfolioCategoryRepository;
    private $portfolioContentRepository;

    /**
     * AboutService constructor.
     * @param PortfolioCategoryRepository $portfolioCategoryRepository
     * @param PortfolioContentRepository $portfolioContentRepository
     */
    public function __construct(PortfolioCategoryRepository $portfolioCategoryRepository, PortfolioContentRepository $portfolioContentRepository)
    {
        $this->portfolioCategoryRepository = $portfolioCategoryRepository;
        $this->portfolioContentRepository = $portfolioContentRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function portfolioCategories()
    {
        $where = ['user_id' => Auth::user()->id];
        $portfolioCategories = $this->portfolioCategoryRepository->getWhere($where);
        foreach ($portfolioCategories as $portfolioCategory){
            $portfolioCategory['filter_key'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioCategories;
    }

    /**
     * @return mixed
     */
    public function portfolioContents()
    {
        $where = ['user_id' => Auth::user()->id];
        $portfolioCategoryIds = $this->portfolioCategoryRepository->pluckWhere($where, 'id');
        $portfolioContents = $this->portfolioContentRepository->whereIn('portfolio_category_id', $portfolioCategoryIds);
        foreach ($portfolioContents as $portfolioContent) {
            $where = ['id' => $portfolioContent->portfolio_category_id];
            $portfolioCategory = $this->portfolioCategoryRepository->whereLast($where);
            $portfolioContent['class'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioContents;
    }

    /**
     * @param $request
     * @return array
     */
    public function storePortfolioCategory($request)
    {
        try{
            $preparedData = $this->preparePortfolioCategoryData($request);
            $this->portfolioCategoryRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Featured Project has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updatePortfolioCategory($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->preparePortfolioCategoryData($request);
            $this->portfolioCategoryRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Featured Project has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function preparePortfolioCategoryData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deletePortfolioCategory($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->portfolioCategoryRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Featured Project has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function storePortfolioContent($request)
    {
        try{
            $preparedData = $this->preparePortfolioContentData($request);
            $this->portfolioContentRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Portfolio Content has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Portfolio Content has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function preparePortfolioContentData($request)
    {
        return [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'url' => $request->url,
            'portfolio_category_id' => $request->portfolio_category_id,
            'image' => uploadFile($request->image, portfolioContentImagePath())
        ];
    }

    /**
     * @param $request
     * @return array
     */
    public function updatePortfolioContent($request)
    {
        try{
            $where = [
                'id' => $request->id,
                'portfolio_category_id' => $request->portfolio_category_id,
            ];
            $preparedData = $this->prepareUpdatedPortfolioContentData($request);
            $this->portfolioContentRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Portfolio Content has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Portfolio Content has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareUpdatedPortfolioContentData($request)
    {
         $preparedData = [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'url' => $request->url,
        ];
        if(isset($request['image'])){
            $where = ['id' => $request->id];
            $oldImage = $this->portfolioContentRepository->pluckWhere($where, 'image');
            $preparedData['image'] = uploadFile($request->image, portfolioContentImagePath(), $oldImage);
        }

        return $preparedData;
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deletePortfolioContent($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $image = $this->portfolioContentRepository->pluckWhere($where, 'image');
            deleteFile(projectImagePath(), image);
            $this->portfolioContentRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Portfolio Content has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Portfolio Content has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
