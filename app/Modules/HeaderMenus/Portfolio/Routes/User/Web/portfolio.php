<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('portfolio', "PortfolioController@portfolio")->name('headerMenu.portfolio');
    Route::post('add-portfolio-category', "PortfolioController@storePortfolioCategory")->name('headerMenu.storePortfolioCategory');
    Route::post('edit-portfolio-category', "PortfolioController@updatePortfolioCategory")->name('headerMenu.updatePortfolioCategory');
    Route::get('delete-portfolio-category/{encryptedId}', "PortfolioController@deletePortfolioCategory")->name('headerMenu.deletePortfolioCategory');
    Route::post('add-portfolio-content', "PortfolioController@storePortfolioContent")->name('headerMenu.storePortfolioContent');
    Route::post('edit-portfolio-content', "PortfolioController@updatePortfolioContent")->name('headerMenu.updatePortfolioContent');
    Route::get('delete-portfolio-content/{encryptedId}', "PortfolioController@deletePortfolioContent")->name('headerMenu.deletePortfolioContent');
});

