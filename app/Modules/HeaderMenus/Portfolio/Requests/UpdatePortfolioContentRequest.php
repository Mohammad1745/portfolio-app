<?php

namespace App\Modules\HeaderMenus\Portfolio\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePortfolioContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'portfolio_category_id' => 'required|numeric',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'url' => 'required|string',
            'image' => isset($this->image) ? 'required|mimes:jpeg,jpg,JPG,png,PNG,gif|max:4000' : '',
        ];
    }
}
