<?php


namespace App\Modules\HeaderMenus\Experiences\Services;

use App\Modules\HeaderMenus\Experiences\Repositories\EducationRepository;
use App\Modules\HeaderMenus\Experiences\Repositories\WorkExperienceRepository;
use Illuminate\Support\Facades\Auth;

class ExperiencesService
{
    private $errorMessage;
    private $errorResponse;
    private $educationRepository;
    private $workExperienceRepository;

    /**
     * ExperienceService constructor.
     * @param EducationRepository $educationRepository
     * @param WorkExperienceRepository $workExperienceRepository
     */
    public function __construct(EducationRepository $educationRepository, WorkExperienceRepository $workExperienceRepository)
    {
        $this->educationRepository = $educationRepository;
        $this->workExperienceRepository = $workExperienceRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function educations()
    {
        $where = ['user_id' => Auth::user()->id];
        $educations = $this->educationRepository->getWhere($where);
        foreach ($educations as $education){
            $education['year_started'] = explode('-', $education->started_at)[0];
            $education['year_ended'] = explode('-', $education->ended_at)[0];
        }

        return $educations;
    }

    /**
     * @return mixed
     */
    public function workExperiences()
    {
        $where = ['user_id' => Auth::user()->id];
        $workExperiences = $this->workExperienceRepository->getWhere($where);
        foreach ($workExperiences as $workExperience){
            $workExperience['responsibilities'] = explode(',', $workExperience->responsibility);
            $workExperience['year_started'] = explode('-', $workExperience->started_at)[0];
            $workExperience['year_ended'] = explode('-', $workExperience->ended_at)[0];
        }

        return $workExperiences;
    }

    /**
     * @param $request
     * @return array
     */
    public function storeEducation($request)
    {
        try{
            $preparedData = $this->prepareEducationData($request);
            $this->educationRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Education has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Education has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateEducation($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareEducationData($request);
            $this->educationRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Education has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Education has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareEducationData($request)
    {
        $preparedData = [
            'user_id' => Auth::user()->id,
            'subject' => $request->subject,
            'institution' => $request->institution,
            'url' => $request->url,
            'description' => $request->description,
            'started_at' => $request->started_at,
        ];
        if($request->ended_at){
            $preparedData['ended_at'] = $request->ended_at;
        }

        return $preparedData;
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteEducation($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->educationRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Education has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Education has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function storeWorkExperience($request)
    {
        try{
            $preparedData = $this->prepareWorkExperienceData($request);
            $this->workExperienceRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Work Experience has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Work Experience has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateWorkExperience($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareWorkExperienceData($request);
            $this->workExperienceRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Work Experience has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Work Experience has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareWorkExperienceData($request)
    {
        $preparedData = [
            'user_id' => Auth::user()->id,
            'job_position' => $request->job_position,
            'company' => $request->company,
            'url' => $request->url,
            'responsibility' => $request->responsibility,
            'started_at' => $request->started_at,
        ];
        if($request->ended_at){
            $preparedData['ended_at'] = $request->ended_at;
        }

        return $preparedData;
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteWorkExperience($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->workExperienceRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Work Experience has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Work Experience has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
