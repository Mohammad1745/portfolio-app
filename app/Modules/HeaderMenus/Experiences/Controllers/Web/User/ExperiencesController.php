<?php

namespace App\Modules\HeaderMenus\Experiences\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Experiences\Requests\StoreEducationRequest;
use App\Modules\HeaderMenus\Experiences\Requests\StoreWorkExperienceRequest;
use App\Modules\HeaderMenus\Experiences\Requests\UpdateEducationRequest;
use App\Modules\HeaderMenus\Experiences\Requests\UpdateWorkExperienceRequest;
use App\Modules\HeaderMenus\Experiences\Services\ExperiencesService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ExperiencesController extends Controller
{
    private $experiencesService;

    /**
     * ExperiencesController constructor.
     * @param ExperiencesService $experiencesService
     */
    public function __construct(ExperiencesService $experiencesService)
    {
        $this->experiencesService = $experiencesService;
    }

    /**
     * @return Application|Factory|View
     */
    public function experiences()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'experiences';
        $data['user'] = Auth::user();
        $data['educations'] = $this->experiencesService->educations();
        $data['workExperiences'] = $this->experiencesService->workExperiences();

        return view('user.header_menus.experiences', $data);
    }

    /**
     * @param StoreEducationRequest $request
     * @return RedirectResponse
     */
    public function storeEducation(StoreEducationRequest $request)
    {
        $response = $this->experiencesService->storeEducation($request);

        return $response['success'] ?
            redirect()->route('headerMenu.experiences')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdateEducationRequest $request
     * @return RedirectResponse
     */
    public function updateEducation(UpdateEducationRequest $request)
    {
        $response = $this->experiencesService->updateEducation($request);

        return $response['success'] ?
            redirect()->route('headerMenu.experiences')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteEducation($encryptedId)
    {
        $response = $this->experiencesService->deleteEducation($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StoreWorkExperienceRequest $request
     * @return RedirectResponse
     */
    public function storeWorkExperience(StoreWorkExperienceRequest $request)
    {
        $response = $this->experiencesService->storeWorkExperience($request);

        return $response['success'] ?
            redirect()->route('headerMenu.experiences')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdateWorkExperienceRequest $request
     * @return RedirectResponse
     */
    public function updateWorkExperience(UpdateWorkExperienceRequest $request)
    {
        $response = $this->experiencesService->updateWorkExperience($request);

        return $response['success'] ?
            redirect()->route('headerMenu.experiences')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteWorkExperience($encryptedId)
    {
        $response = $this->experiencesService->deleteWorkExperience($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
