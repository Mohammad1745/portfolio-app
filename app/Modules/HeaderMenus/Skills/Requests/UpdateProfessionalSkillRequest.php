<?php

namespace App\Modules\HeaderMenus\Skills\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfessionalSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'title' => 'required|string',
            'percentage' => 'required|integer|between:0,100',
        ];
    }
}
