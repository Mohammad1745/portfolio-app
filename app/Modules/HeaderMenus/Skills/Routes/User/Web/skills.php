<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('skills', "SkillsController@skills")->name('headerMenu.skills');
    Route::post('add-technical-skill', "SkillsController@storeTechnicalSkill")->name('headerMenu.storeTechnicalSkill');
    Route::post('edit-technical-skill', "SkillsController@updateTechnicalSkill")->name('headerMenu.updateTechnicalSkill');
    Route::get('delete-technical-skill/{encryptedId}', "SkillsController@deleteTechnicalSkill")->name('headerMenu.deleteTechnicalSkill');
    Route::post('add-professional-skill', "SkillsController@storeProfessionalSkill")->name('headerMenu.storeProfessionalSkill');
    Route::post('edit-professional-skill', "SkillsController@updateProfessionalSkill")->name('headerMenu.updateProfessionalSkill');
    Route::get('delete-professional-skill/{encryptedId}', "SkillsController@deleteProfessionalSkill")->name('headerMenu.deleteProfessionalSkill');
});

