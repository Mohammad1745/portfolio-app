<?php


namespace App\Modules\Profile\Services;

use App\Modules\Profile\Repositories\SharableLinkAccessKeyRepository;
use App\Modules\Profile\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProfileService
{
    private $errorMessage;
    private $errorResponse;
    private $userRepository;
    private $sharableLinkAccessKeyRepository;

    /**
     * ProfileService constructor.
     * @param UserRepository $userRepository
     * @param SharableLinkAccessKeyRepository $sharableLinkAccessKeyRepository
     */
    public function __construct(UserRepository $userRepository, SharableLinkAccessKeyRepository $sharableLinkAccessKeyRepository)
    {
        $this->userRepository = $userRepository;
        $this->sharableLinkAccessKeyRepository = $sharableLinkAccessKeyRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return array
     */
    public function sharableLinks()
    {
        $where = ['user_id' => Auth::user()->id];
        $sharableLinks = $this->sharableLinkAccessKeyRepository->getWhere($where);
        foreach ($sharableLinks as $link){
            $link->url = asset('/portfolio?access_key='.$link['access_key']);
        }

        return $sharableLinks;
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProfile($request)
    {
        try{
            $where = ['id' => Auth::user()->id];
            $updatedUserData = $this->prepareUpdatedData($request);
            $this->userRepository->update($where,$updatedUserData);

            return [
                'success' => true,
                'message' => 'Profile has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Profile has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareUpdatedData($request)
    {
        $preparedData = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'username' => $request->username,
            'email' => $request->email,
            'phone_code' => isset($request->phone_code) ? $request->phone_code : null,
            'phone' => isset($request->phone) ? $request->phone : null,
            'address' => isset($request->address) ? $request->address : null,
            'zip_code' => isset($request->zip_code) ? $request->zip_code : null,
            'city' => isset($request->city) ? $request->city : null,
            'country' => isset($request->country) ? $request->country : null,
        ];
        if(isset($request->photo)){
            $oldImage = Auth::user()->photo;
            $newImage = uploadFile($request->photo, profilePicturePath(), $oldImage);
            $preparedData['photo']  = $newImage;
        }

        return $preparedData;
    }

    /**
     * @return array
     */
    public function generateSharableLink()
    {
        try{
            $linkData = [];
            do{
                $linkData['access_key'] = Str::random(80);
                $object = $this->sharableLinkAccessKeyRepository->whereLast($linkData);
            }while(!empty($object));
            $linkData['user_id'] = Auth::user()->id;
            $linkData['status'] = ACTIVE;
            $this->sharableLinkAccessKeyRepository->create($linkData);

            return [
                'success' => true,
                'message' => 'Sharable Link has been generated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Sharable Link has been generated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteLink($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->sharableLinkAccessKeyRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Link has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Link has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function activateLink($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $data = ['status' => ACTIVE];
            $this->sharableLinkAccessKeyRepository->update($where, $data);

            return [
                'success' => true,
                'message' => 'Link has been activated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Link has been activated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deactivateLink($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $data = ['status' => INACTIVE];
            $this->sharableLinkAccessKeyRepository->update($where, $data);

            return [
                'success' => true,
                'message' => 'Link has been deactivated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Link has been deactivated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
