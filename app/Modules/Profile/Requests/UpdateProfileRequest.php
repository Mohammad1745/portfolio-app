<?php

namespace App\Modules\Profile\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            'email' => 'required|email',
            'photo' => isset($this->photo) ? 'mimes:jpeg,jpg,JPG,png,PNG,gif|max:4000' : '',
            'phone_code' => isset($this->phone_code) ? 'required|string' : '',
            'phone' => isset($this->phone) ? 'required|numeric' : '',
            'address' => isset($this->address) ? 'string' : '',
            'zip_code' => isset($this->zip_code) ? 'numeric' : '',
            'city' => isset($this->city) ? 'string' : '',
            'country' => isset($this->country) ? 'string' : '',
        ];
    }
}
