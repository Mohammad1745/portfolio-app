<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('customize', "CustomizationController@customize")->name('customize');
    Route::get('add-menu/{encryptedId}', "CustomizationController@addMenu")->name('addMenu');
    Route::get('delete-menu/{encryptedId}', "CustomizationController@deleteMenu")->name('deleteMenu');
});

