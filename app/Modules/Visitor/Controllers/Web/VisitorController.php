<?php

namespace App\Modules\Visitor\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Modules\Visitor\Requests\ContactMessageRequest;
use App\Modules\Visitor\Requests\PortfolioRequest;
use App\Modules\Visitor\Services\VisitorService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class VisitorController extends Controller
{
    private $visitorService;

    /**
     * VisitorController constructor.
     * @param VisitorService $visitorService
     */
    public function __construct(VisitorService $visitorService)
    {
        $this->visitorService = $visitorService;
    }

    /**
     * @param PortfolioRequest $request
     * @return Application|Factory|View
     */
    public function portfolio(PortfolioRequest $request)
    {
        $response = $this->visitorService->person($request);
        if($response['success']){
            $data['person'] = $response['data'];
            $data['settings'] = $this->visitorService->settings($response['data']['id']);
            $data['headerMenus'] = $this->visitorService->headerMenus($response['data']['id']);
            $data['profession'] = $this->visitorService->profession($response['data']['id']);
            $data['technicalSkills'] = $this->visitorService->technicalSkills($response['data']['id']);
            $data['professionalSkills'] = $this->visitorService->professionalSkills($response['data']['id']);
            $data['about'] = $this->visitorService->about($response['data']['id']);
            $data['activities'] = $this->visitorService->activities($response['data']['id']);
            $data['featuredProjects'] = $this->visitorService->featuredProjects($response['data']['id']);
            $data['educations'] = $this->visitorService->educations($response['data']['id']);
            $data['workExperiences'] = $this->visitorService->workExperiences($response['data']['id']);
            $data['portfolioCategories'] = $this->visitorService->portfolioCategories($response['data']['id']);
            $data['portfolioContents'] = $this->visitorService->portfolioContents($response['data']['id']);
            $data['prices'] = $this->visitorService->prices($response['data']['id']);
            $data['posts'] = $this->visitorService->posts($response['data']['id']);

            return view('visitor.content', $data);
        }else{
            return redirect(route('errorPage'));
        }
    }

    /**
     * @return Application|Factory|View
     */
    public function errorPage()
    {
        return view('visitor.error');
    }

    /**
     * @param ContactMessageRequest $request
     * @return RedirectResponse
     */
    public function contactMessage(ContactMessageRequest $request)
    {
        $response = $this->visitorService->saveMessage($request);

        return redirect()->back()->with($response['webResponse']);
    }
}
