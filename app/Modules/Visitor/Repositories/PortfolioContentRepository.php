<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\PortfolioContent;

class PortfolioContentRepository extends CommonRepository
{
    /**
     * PortfolioContentRepository constructor.
     */
    public function __construct()
    {
        $model = new PortfolioContent();
        parent::__construct($model);
    }
}
