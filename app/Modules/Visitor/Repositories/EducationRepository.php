<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\Education;

class EducationRepository extends CommonRepository
{
    /**
     * EducationRepository constructor.
     */
    public function __construct()
    {
        $model = new Education();
        parent::__construct($model);
    }
}
