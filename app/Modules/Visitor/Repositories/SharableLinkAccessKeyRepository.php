<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\SharableLinkAccessKey;

class SharableLinkAccessKeyRepository extends CommonRepository
{
    public function __construct()
    {
        $model = new SharableLinkAccessKey();
        parent::__construct($model);
    }
}
