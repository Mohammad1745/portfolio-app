@extends('user.layouts.master')

@section('title')
    Preview - Portfolio
@endsection

@section('content')
@foreach(headerMenus() as $headerMenu)
    @if($headerMenu->menu=='Home')
        <!--
        ===================
           Home
        ===================
        -->
        <section class="mh-home image-bg home-2-img" id="mh-home">
            <div class="img-foverlay img-color-overlay">
                <div class="container">
                    <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">
                        <div class="col-sm-6">
                            <div class="mh-header-info">
                                <div class="mh-promo wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                                    <span>Hello I'm</span>
                                </div>

                                <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">{{$user->first_name.' '.$user->last_name}}</h2>
                                @if(isset($profession->title)) <h4 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">{{$profession->title}}</h4> @endif

                                <ul>
                                    <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s"><i class="fa fa-envelope"></i><a href="mailto:">{{$user->email}}</a></li>
                                    @if(isset($user->phone_code)&&isset($user->phone))<li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s"><i class="fa fa-phone"></i><a href="callto:">{{$user->phone_code.$user->phone}}</a></li>@endif
                                    @if(isset($user->address)&&isset($user->city))<li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s"><i class="fa fa-map-marker"></i><address>{{$user->address}}, {{$user->city}}</address></li>@endif
                                </ul>

                                <ul class="social-icon wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-github"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="hero-img wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                                <div class="img-border">
                                    <img src="{{profilePicture()}}" alt=""  class="h-100 w-100 img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($headerMenu->menu=='About')
        <!--
        ===================
           ABOUT
        ===================
        -->
        <section class="mh-about" id="mh-about">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-about-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                            @if(isset($about->image)) <img src="{{asset(aboutImageViewPath().$about->image)}}" alt="" class="img-fluid">
                            @else<img src="{{asset('assets/images/ab-img.png')}}" alt="" class="img-fluid">
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-about-inner">
                            <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">About Me</h2>
                            <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                @if(isset($about->description)) {{$about->description}} <br>Also, I am good at: @else I am good at: @endif
                            </p>
                            <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                @if(count($technicalSkills)==0)
                                    <h4 class="text-info">No Skill added</h4>
                                @else
                                    <ul>
                                        @foreach($technicalSkills as $technicalSkill)
                                            <li><span>{{$technicalSkill->title}}</span></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <a href="#" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">Downlaod CV <i class="fa fa-download"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--
        ===================
           SERVICE
        ===================
        -->
        @if(count($activities)>0)
            <section class="mh-service">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 text-center section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h2>What I do</h2>
                        </div>
                        @foreach($activities as $activity)
                            <div class="col-sm-4">
                                <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                    <i class="fa fa-object-ungroup sky-color"></i>
                                    <h3>{{$activity->title}}</h3>
                                    <p>{{substr($activity->description, 0, 150)}}...</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

        <!--
        ===================
          FEATURE PROJECTS
        ===================
        -->
        @if(count($featuredProjects)>0)
            <section class="mh-featured-project image-bg featured-img-one">
                <div class="img-color-overlay">
                    <div class="container">
                        <div class="row section-separator">
                            <div class="section-title col-sm-12">
                                <h3>Featured Projects</h3>
                            </div>
                            <div class="col-sm-12">
                                <div class="mh-single-project-slide-by-side row">
                                    <!-- Project Items -->
                                    @foreach($featuredProjects as $project)
                                        <div class="col-sm-12 mh-featured-item">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <div class="mh-featured-project-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                                        <img src="{{asset(projectImageViewPath().$project->image)}}" alt="" class="img-fluid">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="mh-featured-project-content">
                                                        <h4 class="project-category wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">{{$project->project_type}}</h4>
                                                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">{{$project->title}}</h2>
                                                        <span class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">{{$project->subtitle}}</span>
                                                        <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">{{$project->description}}</p>
                                                        <a href="{{$project->url}}" target="_blank" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">View Details</a>

                                                        @if(count($project->comments)>1)
                                                        <div class="mh-testimonial mh-project-testimonial wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.9s">
                                                            @foreach($project->comments as $comment)
                                                                <blockquote>
                                                                    <q>{{$comment->comment}}</q>
                                                                    <cite>- {{$comment->person}}</cite>
                                                                </blockquote>
                                                            @endforeach
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div> <!-- End: .row -->
                    </div>
                </div>
            </section>
        @endif
    @endif
    @if($headerMenu->menu=='Skills')
        <!--
        ===================
           SKILLS
        ===================
        -->
        <section class="mh-skills" id="mh-skills">
            <div class="home-v-img">
                <div class="container">
                    <div class="row section-separator">
                        <div class="section-title text-center col-sm-12">
                            <!--<h2>Skills</h2>-->
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="mh-skills-inner">
                                <div class="mh-professional-skill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                    <h3>Technical Skills</h3>
                                    <div class="each-skills">
                                        @if(count($technicalSkills)==0)
                                            <h4 class="text-info center">No Skill added</h4>
                                        @else
                                            @foreach($technicalSkills as $technicalSkill)
                                                <div class="candidatos">
                                                    <div class="parcial">
                                                        <div class="info">
                                                            <div class="nome">{{$technicalSkill->title}}</div>
                                                            <div class="percentagem-num">{{$technicalSkill->percentage}}%</div>
                                                        </div>
                                                        <div class="progressBar">
                                                            <div class="percentagem" style="width: {{$technicalSkill->percentage}}%;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="mh-professional-skills wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
                                <h3>Professional Skills</h3>
                                <ul class="mh-professional-progress">
                                    @if(count($professionalSkills)==0)
                                        <h4 class="text-info center">No Skill</h4>
                                    @else
                                        @foreach($professionalSkills as $professionalSkill)
                                            <li>
                                                <div class="mh-progress mh-progress-circle" data-progress="{{$professionalSkill->percentage}}"></div>
                                                <div class="pr-skill-name">{{$professionalSkill->title}}</div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($headerMenu->menu=='Experiences')
        <!--
        ===================
           EXPERIENCES
        ===================
        -->
        <section class="mh-experince image-bg featured-img-one" id="mh-experience">
            <div class="img-color-overlay">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 col-md-6">
                            <div class="mh-education">
                                <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Education</h3>

                                <div class="mh-education-deatils">
                                    @if(count($educations)==0)
                                        <h4 class="text-info">Not added</h4>
                                    @else
                                        @foreach($educations as $education)
                                        <!-- Education Institutes-->
                                            <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                                <h4>{{$education->subject}} From <a href="{{$education->url}}" target="_blank">{{$education->institution}}</a></h4>
                                                <div class="mh-eduyear">{{$education->year_started}}-{{$education->year_ended}}</div>
                                                <p>{{$education->description}}</p>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="mh-work">
                                <h3>Work Experience</h3>

                                <div class="mh-experience-deatils">
                                    @if(count($workExperiences)==0)
                                        <h4 class="text-info">Not added</h4>
                                    @else
                                        @foreach($workExperiences as $workExperience)
                                        <!-- Work Experience-->
                                            <div class="mh-work-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                                                <h4>{{$workExperience->job_position}} <a href="{{$workExperience->url}}">{{$workExperience->company}}</a></h4>
                                                <div class="mh-eduyear">{{$workExperience->year_started}}-{{$workExperience->year_ended}}</div>
                                                <span>Responsibility :</span>
                                                <ul class="work-responsibility">
                                                    @foreach($workExperience->responsibilities as $responsibility)
                                                        <li><i class="fa fa-circle"></i>{{$responsibility}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($headerMenu->menu=='Portfolio')
        @if(count($portfolioCategories)>0)
            <section class="mh-portfolio" id="mh-portfolio">
                <div class="container">
                    <div class="row section-separator">
                        <div class="section-title col-sm-12 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                            <h3>Recent Portfolio</h3>
                        </div>
                        <div class="part col-sm-12">
                            <div class="portfolio-nav col-sm-12" id="filter-button">
                                <ul>
                                    <li data-filter="*" class="current wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s"> <span>All Categories</span></li>
                                    @foreach($portfolioCategories as $portfolioCategory)
                                        <li data-filter=".{{$portfolioCategory->filter_key}}" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                            <span>{{$portfolioCategory->title}}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="mh-project-gallery col-sm-12 wow fadeInUp" id="project-gallery" data-wow-duration="0.8s" data-wow-delay="0.5s">
                                <div class="portfolioContainer row">
                                    @if(count($portfolioContents)==0)
                                        <h4 class="text-info center">No Content Added</h4>
                                    @else
                                        @foreach($portfolioContents as $portfolioContent)
                                            <div class="grid-item col-md-4 col-sm-6 col-xs-12 {{$portfolioContent->class}}">
                                                <figure>
                                                    <img src="{{asset(portfolioContentImageViewPath() . $portfolioContent->image)}}" alt="img04">
                                                    <figcaption class="fig-caption">
                                                        <i class="fa fa-search"></i>
                                                        <h5 class="title">{{$portfolioContent->title}}</h5>
                                                        <span class="sub-title">{{$portfolioContent->subtitle}}</span>
                                                        <a href="{{$portfolioContent->url}}" target="_blank"></a>
                                                        {{--                                                <a data-fancybox data-src="#mh"></a>--}}
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        @endforeach
                                    @endif
                                </div> <!-- End: .grid .project-gallery -->
                            </div> <!-- End: .grid .project-gallery -->
                        </div> <!-- End: .part -->
                    </div> <!-- End: .row -->
                </div>
                <div class="mh-portfolio-modal" id="mh">
                    <div class="container">
                        <div class="row mh-portfolio-modal-inner">
                            <div class="col-sm-5">
                                <h2>Wrap - A campanion plugin</h2>
                                <p>Wrap is a clean and elegant Multipurpose Landing Page Template.
                                    It will fit perfectly for Startup, Web App or any type of Web Services.
                                    It has 4 background styles with 6 homepage styles. 6 pre-defined color scheme.
                                    All variations are organized separately so you can use / customize the template very easily.</p>

                                <p>It is a clean and elegant Multipurpose Landing Page Template.
                                    It will fit perfectly for Startup, Web App or any type of Web Services.
                                    It has 4 background styles with 6 homepage styles. 6 pre-defined color scheme.
                                    All variations are organized separately so you can use / customize the template very easily.</p>
                                <div class="mh-about-tag">
                                    <ul>
                                        <li><span>php</span></li>
                                        <li><span>html</span></li>
                                        <li><span>css</span></li>
                                        <li><span>php</span></li>
                                        <li><span>wordpress</span></li>
                                        <li><span>React</span></li>
                                        <li><span>Javascript</span></li>
                                    </ul>
                                </div>
                                <a href="#" class="btn btn-fill">Live Demo</a>
                            </div>
                            <div class="col-sm-7">
                                <div class="mh-portfolio-modal-img">
                                    <img src="{{asset('assets/images/pr-0.jif')}}" alt="" class="img-fluid">
                                    <p>All variations are organized separately so you can use / customize the template very easily.</p>
                                    <img src="{{asset('assets/images/pr-1.jif')}}" alt="" class="img-fluid">
                                    <p>All variations are organized separately so you can use / customize the template very easily.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--
            ===================
               QUATES
            ===================
            -->
            <section class="mh-quates image-bg home-2-img">
                <div class="img-color-overlay">
                    <div class="container">
                        <div class="row section-separator">
                            <div class="each-quates col-sm-12 col-md-6">
                                <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Interested to Work?</h3>
                                <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Mirum est notare quam littera gothica.
                                    quam nunc putamus parum claram,</p>
                                <a href="#mh-contact" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endif
    @if($headerMenu->menu=='Pricing')
        <!--
        ===================
           PRICING
        ===================
        -->
        <section class="mh-pricing" id="mh-pricing">
            <div class="">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h3>Pricing Table</h3>
                        </div>
                        @if(count($prices)==0)
                            <h4 class="text-info center">No Price Added</h4>
                        @else
                            @foreach($prices as $price)
                                <div class="col-sm-12 col-md-4">
                                    <div class="mh-pricing dark-bg shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                        <i class="fa fa-calendar"></i>
                                        <h4>{{$price->title}}
                                        </h4>
                                        <p>{{$price->subtitle}}</p>
                                        <h5>${{$price->amount}}</h5>
                                        <ul>
                                            @foreach($price->contents as $content)
                                                <li>{{$content}}</li>
                                            @endforeach
                                        </ul>
                                        @if($price->status)<a href="{{$price->url}}" target="_blank" class="btn btn-fill">Hire Me</a>
                                        @else <p class="text-muted">Hired</p> @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($headerMenu->menu=='Blog')
        <!--
        ===================
           BLOG
        ===================
        -->
        <section class="mh-blog image-bg featured-img-two" id="mh-blog">
            <div class="img-color-overlay">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h3>Featured Posts</h3>
                        </div>
                        @if(count($posts)==0)
                            <h4 class="text-info center">No Post Added</h4>
                        @else
                            @foreach($posts as $post)
                                <div class="col-sm-12 col-md-4">
                                    <div class="mh-blog-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                        <img src="{{asset(blogImageViewPath().$post->image)}}" alt="" class="img-fluid">
                                        <div class="blog-inner">
                                            <h2><a href="{{$post->url}}" target="_blank">{{$post->title}}</a></h2>
                                            <div class="mh-blog-post-info">
                                                <ul>
                                                    <li><strong>Post On</strong><a href="#">{{date_format($post->created_at, 'd.m.y')}}</a></li>
                                                    <li><strong>By</strong><a href="#">{{$post->site}}</a></li>
                                                </ul>
                                            </div>
                                            <p>{{substr($post->content, 0, 200)}}</p>
                                            <a href="{{$post->url}}" target="_blank">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($headerMenu->menu=='Contact')
        <!--
        ===================
           FOOTER 1
        ===================
        -->
        <footer class="mh-footer" id="mh-contact">
            <div class="map-image image-bg">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h3>Contact Me</h3>
                        </div>
                        <div class="col-sm-12 mh-footer-address">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 mh-footer-address">
                                    <div class="col-sm-12 xs-no-padding">
                                        <div class="mh-address-footer-item dark-bg shadow-1 media wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                            <div class="each-icon">
                                                <i class="fa fa-location-arrow"></i>
                                            </div>
                                            <div class="each-info media-body">
                                                <h4>Address</h4>
                                                <address>
                                                    {{$user->address}}, <br>
                                                    {{$user->city}}
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 xs-no-padding">
                                        <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                                            <div class="each-icon">
                                                <i class="fa fa-envelope-o"></i>
                                            </div>
                                            <div class="each-info media-body">
                                                <h4>Email</h4>
                                                <a href="mailto:{{$user->email}}">{{$user->email}}</a><br>
                                                <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 xs-no-padding">
                                        <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                                            <div class="each-icon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <div class="each-info media-body">
                                                <h4>Phone</h4>
                                                <a href="callto:{{$user->phone_code}}{{$user->phone}}">({{$user->phone_code}}) {{$user->phone}}</a><br>
                                                <a href="callto:{{$user->phone_code}}{{$user->phone}}">({{$user->phone_code}}) {{$user->phone}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                    {{ Form::open(['route' => 'contactMessage', 'method' => 'post']) }}
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <div class="col-sm-12">
                                        <input name="first_name" class="contact-name form-control" id="first-name" type="text" placeholder="First Name" required>
                                        @if (isset($errors) && $errors->has('first_name'))
                                            <span class="text-danger"><strong>{{ $errors->first('first_name') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12">
                                        <input name="last_name" class="contact-email form-control" id="last-name" type="text" placeholder="Last Name" required>
                                        @if (isset($errors) && $errors->has('last_name'))
                                            <span class="text-danger"><strong>{{ $errors->first('last_name') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12">
                                        <input name="email" class="contact-subject form-control" id="email" type="email" placeholder="Your Email" required>
                                        @if (isset($errors) && $errors->has('email'))
                                            <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12">
                                        <textarea name="message" class="contact-message" id="message" rows="6" placeholder="Your Message" required></textarea>
                                        @if (isset($errors) && $errors->has('message'))
                                            <span class="text-danger"><strong>{{ $errors->first('message') }}</strong></span>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-fill btn-block" id="form-submit">Send Message</button>
                                    {{Form::close()}}
                                </div>
                                <div class="col-sm-12 mh-copyright wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="text-left text-xs-center">
                                                <p><a href="templateshub.net">Templates Hub</a></p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul class="social-icon">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-github"></i></a></li>
                                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    @endif
@endforeach
@endsection
