@extends('user.layouts.master')

@section('title')
    Messages - Account - Portfolio
@endsection

@section('content')
    <footer class="mh-footer" id="mh-contact">
        <div class="map-image image-bg">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 mh-footer-address">
                        <table id="message-table" class="table message-table table-bordered text-center mb-0">
                            <thead>
                            <tr>
                                <th class="all">{{__('Name')}}</th>
                                <th class="all">{{__('Email')}}</th>
                                <th class="all">{{__('Message')}}</th>
                                <th class="all">{{__('Actions')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('script')
    <script>
        $('#message-table').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            bLengthChange: true,
            responsive:true,
            sScrollX: "100%",
            ajax: '{{route('account.getMessageList')}}',
            order:[1,'asc'],
            autoWidth:false,
            language: {
                paginate: {
                    next: 'Next &#8250;',
                    previous: '&#8249; Previous'
                },
                search: "_INPUT_",
                searchPlaceholder: "Search...",
            },
            columnDefs: [
                {"className": "text-center", "targets": "_all"}
            ],
            columns: [
                {"data": "name"},
                {"data": "email"},
                {"data": "message"},
                {"data": "actions", orderable: false, searchable: false}
            ]
        });
    </script>
@endsection
