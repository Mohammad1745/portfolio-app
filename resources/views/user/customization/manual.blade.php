@extends('user.layouts.master')

@section('title')
    Customization Manual - Portfolio
@endsection

@section('content')
    <section class="mh-home image-bg home-2-img" id="mh-home">
        <div class="img-foverlay img-color-overlay">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">
                    <div class="col-sm-12 col-md-12">
                        <div class="mh-about-inner">
                            <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">Customize portfolio</h2>
                            <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Hello,
                                You may customize your portfolio layout and content as you wish.<br>
                                How to do that:</p>
                            <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                <ol>
                                    <li><span>Click Avatar icon on top-right corner for you profile.</span></li>
                                    <li><span>Click <i class="fa fa-plus"></i> icon to add content</span></li>
                                    <li><span>Click <i class="fa fa-edit"></i> icon to edit content</span></li>
                                    <li><span>Click <i class="fa fa-trash"></i> icon to delete content</span></li>
                                    <li><span>Click any of the added menu to add or change its content.</span></li>
                                </ol>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
