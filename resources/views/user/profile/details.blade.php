@extends('user.layouts.master')

@section('title')
    Profile - Portfolio
@endsection

@section('content')
    <section class="mh-home image-bg home-2-img" id="mh-home">
        <div class="img-foverlay img-color-overlay">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content profile-padding">
                    <div class="col-md-7 col-sm-12 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">Profile
                            <a title="Edit Profile" id="profile-edit-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                        </h2>
                        <br>
                        <div style="font-size: 20px;">
                            <span class="row"><h4 class="col-md-4">Name </h4><h4 class="col-md-8 pl-1">{{$user->first_name.' '.$user->last_name}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Username </h4><h4 class="col-md-8 pl-1">{{$user->username}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Email </h4><h4 class="col-md-8 pl-1">{{$user->email}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Phone </h4><h4 class="col-md-8 pl-1">{{'('.$user->phone_code .')'.$user->phone}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Address </h4><h4 class="col-md-8 pl-1">{{$user->address}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Zip Code </h4><h4 class="col-md-8 pl-1">{{$user->zip_code}}</h4></span>
                            <span class="row"><h4 class="col-md-4">City </h4><h4 class="col-md-8 pl-1">{{$user->city}}</h4></span>
                            <span class="row"><h4 class="col-md-4">Country </h4><h4 class="col-md-8 pl-1">{{$user->country}}</h4></span>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 wow fadeInUp">
                        <div class="hero-img wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                            <div class="img-border">
                                <img src="{{profilePicture()}}" alt="" class="h-100 w-100 img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 wow fadeInUp">
                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">Sharable Links <span class="text-muted" style="font-size: small">(Click any link to copy)</span> </h2>
                        @foreach($sharableLinks as $link)
                            @if($link->status) <span title="Click & Copy" class="copy-button cursor-pointer" style="line-height: 1px;">{{$link->url}}</span>
                            @else <span title="Inactive Link" class="text-muted" style="line-height: 1px;">{{$link->url}}</span>
                            @endif
                            <a title="Delete Sharable Link" href="{{route('deleteSharableLink', encrypt($link->id))}}" class="price-delete-button ml-2 text-muted float-right mr-2" style="font-size: small"
                               data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                            <a @if($link->status) title="Deactivate Sharable Link" href="{{route('deactivateSharableLink', encrypt($link->id))}}"
                               @else title="Activate Sharable Link" href="{{route('activateSharableLink', encrypt($link->id))}}" @endif
                               class="text-muted float-right" style="font-size: small"> <i class="fa fa-check"></i> </a>
                        @endforeach
                        <br><a href="{{route('generateSharableLink')}}" class="text-muted" style="font-size: medium"> Generate Sharable Link </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hidden Form To Edit Profile -->
    <div id="edit-profile-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>Edit Profile</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'updateProfile', 'method' => 'post', 'files' => 'true']) }}
        <div class="row">
            <div class="row col-md-8 col-sm-12">
                <div class="form-group col-md-6 col-sm-12">
                    <label for="first_name">{{__('First Name')}}</label>
                    <input name="first_name" id="first_name" class="form-control" type="text" @if(isset($user->first_name)) value="{{$user->first_name}}" @else value="{{old('first_name')}}" @endif>
                    @if (isset($errors) && $errors->has('first_name'))
                        <span class="text-danger"><strong>{{ $errors->first('first_name') }}</strong></span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-12">
                    <label for="last_name">{{__('Last Name')}}</label>
                    <input name="last_name" id="last_name" class="form-control form-control-rounded" type="text" @if(isset($user->last_name)) value="{{$user->last_name}}" @else value="{{old('last_name')}}" @endif>
                    @if (isset($errors) && $errors->has('last_name'))
                        <span class="text-danger"><strong>{{ $errors->first('last_name') }}</strong></span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-12">
                    <label for="username">{{__('Username')}}</label>
                    <input name="username" id="username" class="form-control form-control-rounded" type="text" @if(isset($user->username)) value="{{$user->username}}" @else value="{{old('username')}}" @endif>
                    @if (isset($errors) && $errors->has('username'))
                        <span class="text-danger"><strong>{{ $errors->first('username') }}</strong></span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-12">
                    <label for="email">{{__('Email')}}</label>
                    <input name="email" id="email" class="form-control form-control-rounded" type="email" @if(isset($user->email)) value="{{$user->email}}" @else value="{{old('email')}}" @endif>
                    @if (isset($errors) && $errors->has('email'))
                        <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-12">
                    <label for="phone_code">{{__('Phone Code')}}</label>
                    <input name="phone_code" id="phone_code" class="form-control form-control-rounded" type="text" @if(isset($user->phone_code)) value="{{$user->phone_code}}" @else value="{{old('phone_code')}}" @endif>
                    @if (isset($errors) && $errors->has('phone_code'))
                        <span class="text-danger"><strong>{{ $errors->first('phone_code') }}</strong></span>
                    @endif
                </div>
                <div class="form-group col-md-6 col-sm-12">
                    <label for="phone">{{__('Phone')}}</label>
                    <input name="phone" id="phone" class="form-control form-control-rounded" type="text" @if(isset($user->phone)) value="{{$user->phone}}" @else value="{{old('phone')}}" @endif>
                    @if (isset($errors) && $errors->has('phone'))
                        <span class="text-danger"><strong>{{ $errors->first('phone') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="row col-md-4 col-sm-12 pr-0">
                <div class="form-group col-md-12 pr-0">
                    <label>{{__('Profile Picture')}}</label>
                    <input name="photo" type="file" id="input-file-now" class="dropify"
                           data-default-file="{{isset($user->photo) && !empty($user->photo) ? asset(profilePictureViewPath().$user->photo) : ''}}" >
                    @if (isset($errors) && $errors->has('photo'))
                        <div class="text-danger">{{ $errors->first('photo') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="address">{{__('Address')}}</label>
            <input name="address" id="address" class="form-control form-control-rounded" type="text" @if(isset($user->address)) value="{{$user->address}}" @else value="{{old('address')}}" @endif>
            @if (isset($errors) && $errors->has('address'))
                <span class="text-danger"><strong>{{ $errors->first('address') }}</strong></span>
            @endif
        </div>
        <div class="row">
            <div class="form-group col-md-4 col-sm-12">
                <label for="zip_code">{{__('Zip Code')}}</label>
                <input name="zip_code" id="zip_code" class="form-control form-control-rounded" type="text" @if(isset($user->zip_code)) value="{{$user->zip_code}}" @else value="{{old('zip_code')}}" @endif>
                @if (isset($errors) && $errors->has('zip_code'))
                    <span class="text-danger"><strong>{{ $errors->first('zip_code') }}</strong></span>
                @endif
            </div>
            <div class="form-group col-md-4 col-sm-12">
                <label for="city">{{__('City')}}</label>
                <input name="city" id="city" class="form-control form-control-rounded" type="text" @if(isset($user->city)) value="{{$user->city}}" @else value="{{old('city')}}" @endif>
                @if (isset($errors) && $errors->has('city'))
                    <span class="text-danger"><strong>{{ $errors->first('city') }}</strong></span>
                @endif
            </div>
            <div class="form-group col-md-4 col-sm-12">
                <label for="country">{{__('country')}}</label>
                <input name="country" id="country" class="form-control form-control-rounded" type="text" @if(isset($user->country)) value="{{$user->country}}" @else value="{{old('country')}}" @endif>
                @if (isset($errors) && $errors->has('country'))
                    <span class="text-danger"><strong>{{ $errors->first('country') }}</strong></span>
                @endif
            </div>
        </div>

        <button class="btn btn-fill btn-block mt-2">{{__('Update Profile')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('assets/js/dropify.min.js') }}'></script>
    <script>
        $(document).ready(function () {
            $('.dropify').dropify();
        });
    </script>
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#profile-edit-button').on('click', function () {
                $('.hidden').hide();
                $('#edit-profile-form').show('slow');
            });
            //Copy to clipboard
            $('.copy-button').on('click', function () {
                let $temp = $("<input>");
                $("body").append($temp);
                $temp.val($(this).text()).select();
                document.execCommand("copy");
                $temp.remove();
                alert('Copied to clipboard!');
            });
        });
    </script>
@endsection
