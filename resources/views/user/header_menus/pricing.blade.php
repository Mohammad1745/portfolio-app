@extends('user.layouts.master')

@section('title')
    Pricing - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-pricing" id="mh-pricing">
        <div class="">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h3>Pricing Table
                            <a title="Add Price" id="price-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                        </h3>
                    </div>
                    @if(count($prices)==0)
                        <h4 class="text-info center">No Price Added</h4>
                    @else
                        @foreach($prices as $price)
                            <div class="col-sm-12 col-md-4">
                                <a title="Delete Price" href="{{route('headerMenu.deletePrice', encrypt($price->id))}}" class="price-delete-button ml-1 text-muted float-right mr-2" style="font-size: small"
                                   data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                <a title="Edit Price" data-info="{{$price}}" class="price-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                <a @if($price->status) title="Deactivate Price" href="{{route('headerMenu.deactivatePrice', encrypt($price->id))}}"
                                   @else title="Activate Price" href="{{route('headerMenu.activatePrice', encrypt($price->id))}}" @endif
                                   class="price-delete-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-check"></i> </a>
                                <div class="mh-pricing dark-bg shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                    <i class="fa fa-calendar"></i>
                                    <h4>{{$price->title}}
                                    </h4>
                                    <p>{{$price->subtitle}}</p>
                                    <h5>${{$price->amount}}</h5>
                                    <ul>
                                        @foreach($price->contents as $content)
                                            <li>{{$content}}</li>
                                        @endforeach
                                    </ul>
                                    @if($price->status)<a href="{{$price->url}}" target="_blank" class="btn btn-fill">Hire Me</a>
                                    @else <p class="text-muted">Hired</p> @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- Hidden Form To Add Price -->
    <div id="add-price-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>New Price</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storePrice', 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="{{old('subtitle')}}" placeholder="">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Status')}}</label>
                    <select name="status" id="status" class="form-control form-control-rounded ">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    @if (isset($errors) && $errors->has('status'))
                        <div class="text-danger">{{ $errors->first('status') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Price($)')}}</label>
                    <input name="price" id="price" class="form-control form-control-rounded" type="number" step="0.001" min="0" value="{{old('price')}}" placeholder="">
                    @if (isset($errors) && $errors->has('price'))
                        <span class="text-danger"><strong>{{ $errors->first('price') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="content">{{__('Content')}}</label>
            <textarea name="content" id="content" class="form-control form-control-rounded" placeholder="Add comma(,) separated contents" type="text">{{old('content')}}</textarea>
            @if (isset($errors) && $errors->has('content'))
                <span class="text-danger"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Price -->
    <div id="edit-price-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>Edit Price</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updatePrice', 'method' => 'post']) }}
        <input type="hidden" name="id" id="id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="" placeholder="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="" placeholder="">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Status')}}</label>
                    <select name="status" id="status" class="form-control form-control-rounded">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    @if (isset($errors) && $errors->has('status'))
                        <div class="text-danger">{{ $errors->first('status') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Price($)')}}</label>
                    <input name="price" id="price" class="form-control form-control-rounded" type="number" step="0.001" min="0" value="" placeholder="">
                    @if (isset($errors) && $errors->has('price'))
                        <span class="text-danger"><strong>{{ $errors->first('price') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="" placeholder="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="content">{{__('Content')}}</label>
            <textarea name="content" id="content" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('content'))
                <span class="text-danger"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#price-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-price-form').show('slow');
            });
            $('.price-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-price-form').show('slow');
                $('#edit-price-form').find('#id').val(info['id']);
                $('#edit-price-form').find('#title').val(info['title']);
                $('#edit-price-form').find('#subtitle').val(info['subtitle']);
                $('#edit-price-form').find('#status').val(info['status']);
                $('#edit-price-form').find('#url').val(info['url']);
                $('#edit-price-form').find('#price').val(info['amount']);
                $('#edit-price-form').find('#content').html(info['content']);
            });
        });
    </script>
@endsection
