@extends('user.layouts.master')

@section('title')
    Contact - Header Menu - Portfolio
@endsection

@section('content')
    <footer class="mh-footer" id="mh-contact">
        <div class="map-image image-bg">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h3>Contact Me</h3>
                    </div>
                    <div class="col-sm-12 mh-footer-address">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 mh-footer-address">
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item dark-bg shadow-1 media wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                        <div class="each-icon">
                                            <i class="fa fa-location-arrow"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Address
                                                <a title="Edit on profile" class="text-muted float-right" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                            </h4>
                                            <address>
                                                {{$user->address}}, <br>
                                                {{$user->city}}
                                            </address>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                                        <div class="each-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Email
                                                <a title="Edit on profile" class="text-muted float-right" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                            </h4>
                                            <a href="mailto:{{$user->email}}">{{$user->email}}</a><br>
                                            <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 xs-no-padding">
                                    <div class="mh-address-footer-item media dark-bg shadow-1 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                                        <div class="each-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="each-info media-body">
                                            <h4>Phone
                                                <a title="Edit on profile" class="text-muted float-right" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                            </h4>
                                            <a href="callto:{{$user->phone_code}}{{$user->phone}}">({{$user->phone_code}}) {{$user->phone}}</a><br>
                                            <a href="callto:{{$user->phone_code}}{{$user->phone}}">({{$user->phone_code}}) {{$user->phone}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                <form id="contactForm" class="single-form quate-form wow fadeInUp" data-toggle="validator">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input name="name" class="contact-name form-control" id="name" type="text" placeholder="First Name" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <input name="name" class="contact-email form-control" id="L_name" type="text" placeholder="Last Name" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <input name="name" class="contact-subject form-control" id="email" type="email" placeholder="Your Email" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <textarea class="contact-message" id="message" rows="6" placeholder="Your Message" required></textarea>
                                        </div>

                                        <!-- Subject Button -->
                                        <div class="btn-form col-sm-12">
                                            <button type="submit" class="btn btn-fill btn-block" id="form-submit">Send Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-12 mh-copyright wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="text-left text-xs-center">
                                            <p><a href="templateshub.net">Templates Hub</a></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="social-icon">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('script')
{{--    <script>--}}
{{--        $('.hidden').hide();--}}
{{--        $('document').ready(function () {--}}
{{--            $('#profession-edit-button').on('click', function () {--}}
{{--                $('#profession-form').show('slow');--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@endsection
