@extends('user.layouts.master')

@section('title')
    About - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-about" id="mh-about">
        <div class="container">
            <div class="row section-separator">
                <div class="col-sm-12 col-md-6">
                    <div class="mh-about-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                        @if(isset($about->image)) <img src="{{asset(aboutImageViewPath().$about->image)}}" alt="" class="img-fluid">
                        @else<img src="{{asset('assets/images/ab-img.png')}}" alt="" class="img-fluid">
                        @endif
                    </div>
                    <a title="Edit" class="description-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="mh-about-inner">
                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">About Me</h2>
                        <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            @if(isset($about->description)) {{$about->description}} @else Write Something about yourself. @endif
                            <a title="Edit" class="description-edit-button ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                <br>Also, I am good at:
                        </p>
                        <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                            @if(count($technicalSkills)==0)
                                <h4 class="text-info">No Skill added</h4>
                            @else
                                <ul>
                                    @foreach($technicalSkills as $technicalSkill)
                                        <li><span>{{$technicalSkill->title}}</span></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                        <a href="#" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">Downlaod CV <i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       SERVICE
    ===================
    -->
    <section class="mh-service">
        <div class="container">
            <div class="row section-separator">
                <div class="col-sm-12 text-center section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                    <h2>What I do
                        <a title="Add Activities" id="activity-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                    </h2>
                </div>
                @if(count($activities)==0)
                    <h4 class="text-info center">No Activity Added</h4>
                @else
                    @foreach($activities as $activity)
                        <div class="col-sm-4">
                            <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                <i class="fa fa-object-ungroup sky-color"></i>
                                <a title="Delete {{$activity->title}}" href="{{route('headerMenu.deleteActivity', encrypt($activity->id))}}" class="activity-delete-button ml-1 text-muted float-right" style="font-size: small"
                                   data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                <a title="Edit {{$activity->title}}" data-info="{{$activity}}" class="activity-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                <h3>{{$activity->title}}</h3>
                                <p>{{substr($activity->description, 0, 150)}}...</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <!--
    ===================
      FEATURE PROJECTS
    ===================
    -->
    <section class="mh-featured-project image-bg featured-img-one">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="section-title col-sm-12">
                        <h3>Featured Projects
                            <a title="Add Featured Project" id="featured-project-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                        </h3>
                    </div>
                    <div class="col-sm-12">
                        <div class="mh-single-project-slide-by-side row">
                            <!-- Project Items -->
                            @if(count($featuredProjects)==0)
                                <h4 class="text-info center">No Project Added</h4>
                            @else
                                @foreach($featuredProjects as $project)
                                    <div class="col-sm-12 mh-featured-item">
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="mh-featured-project-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                                    <img src="{{asset(projectImageViewPath().$project->image)}}" alt="" class="img-fluid">
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="mh-featured-project-content">
                                                    <h4 class="project-category wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">{{$project->project_type}}</h4>
                                                    <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">{{$project->title}}
                                                        <a title="Delete Project" href="{{route('headerMenu.deleteFeaturedProject', encrypt($project->id))}}" class="ml-1 text-muted float-right" style="font-size: small"
                                                           data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                                        <a title="Edit Project" data-info="{{$project}}" class="featured-project-edit-button wow fadeInUp ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                                    </h2>
                                                    <span class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">{{$project->subtitle}}</span>
                                                    <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">{{$project->description}}</p>
                                                    <a href="{{$project->url}}" target="_blank" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">View Details</a>
                                                    <br><a title="Add Project Comment" data-info="{{$project}}" class="project-comment-add-button ml-1 text-muted wow fadeInUp" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                                                    <!-- Comments -->
                                                    @if(count($project->comments)<1)
                                                    <p>Add others comment</p>
                                                    @else
                                                        <div class="wow fadeInUp overflow-scroll-y" @if(count($project->comments)>2) style="height: 100px;" @endif data-wow-duration="0.8s" data-wow-delay="0.9s">
                                                            @foreach($project->comments as $comment)
                                                                <blockquote>
                                                                    <q>{{$comment->comment}}</q>
                                                                    <cite>- {{$comment->person}}</cite>
                                                                    <a title="Edit Comment" data-info="{{$comment}}" class="project-comment-edit-button ml-1 text-muted wow fadeInUp" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                                                    <a title="Delete Comment" href="{{route('headerMenu.deleteProjectComment', encrypt($comment->id))}}" class="ml-1 text-muted" style="font-size: small"
                                                                       data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                                                </blockquote>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div> <!-- End: .row -->
            </div>
        </div>
    </section>
    <!-- Hidden Form To Edit Description -->
    <div id="edit-description-form" class="col-8 hidden float-bottom-right overflow-scroll-y">
        <h5>Edit Description</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateDescription', 'method' => 'post', 'files' => 'true']) }}
        <div class="form-group">
            <label>{{__('Image')}}<span class="">(Optional)</span></label>
            <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
            @if (isset($errors) && $errors->has('image'))
                <div class="text-danger">{{ $errors->first('image') }}</div>
            @endif
        </div>
        <div class="form-group">
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text">@if(isset($about->description)){{$about->description}} @else {{old('description')}} @endif</textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Activity -->
    <div id="add-activity-form" class="col-8 hidden float-bottom-left">
        <h5>New Activity</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeActivity', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">{{__('Title')}}</label>
            <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="">
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">{{__('Description')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text">{{old('description')}}</textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Activity -->
    <div id="edit-activity-form" class="col-8 hidden float-bottom-right">
        <h5>Edit Activity</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateActivity', 'method' => 'post']) }}
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="">
            <input name="title" id="title" class="form-control form-control-rounded" type="text"  value="" >
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">{{__('Description')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Featured Project -->
    <div id="add-featured-project-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>New Featured Project</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeFeaturedProject', 'method' => 'post', 'files' => 'true']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="project_type">{{__('Project Type')}}</label>
                    <input name="project_type" id="project_type" class="form-control form-control-rounded" type="text" value="{{old('project_type')}}" placeholder="">
                    @if (isset($errors) && $errors->has('project_type'))
                        <span class="text-danger"><strong>{{ $errors->first('project_type') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="{{old('subtitle')}}" placeholder="">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description">{{__('Short Note')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text">{{old('description')}}</textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Featured Project -->
    <div id="edit-featured-project-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>New Featured Project</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateFeaturedProject', 'method' => 'post', 'files' => 'true']) }}
        <input type="hidden" name="id" id="id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="project_type">{{__('Project Type')}}</label>
                    <input name="project_type" id="project_type" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('project_type'))
                        <span class="text-danger"><strong>{{ $errors->first('project_type') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description">{{__('Short Note')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Project Comment -->
    <div id="add-project-comment-form" class="col-8 hidden float-bottom-right">
        <h5>New Project Comment</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeProjectComment', 'method' => 'post']) }}
        <div class="form-group">
            <input type="hidden" name="user_featured_project_id" id="user_featured_project_id" value="">
            <label for="person">{{__('Person')}}</label>
            <input name="person" id="person" class="form-control form-control-rounded" type="text" value="{{old('person')}}" placeholder="">
            @if (isset($errors) && $errors->has('person'))
                <span class="text-danger"><strong>{{ $errors->first('person') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="comment">{{__('Comment')}}</label>
            <textarea name="comment" id="comment" class="form-control form-control-rounded" type="text">{{old('comment')}}</textarea>
            @if (isset($errors) && $errors->has('comment'))
                <span class="text-danger"><strong>{{ $errors->first('comment') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow fadeInUp btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Project Comment -->
    <div id="edit-project-comment-form" class="col-8 hidden float-bottom-right">
        <h5>Edit Project Comment</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateProjectComment', 'method' => 'post']) }}
        <div class="form-group">
            <input type="hidden" name="id" id="id" value="">
            <input type="hidden" name="user_featured_project_id" id="user_featured_project_id" value="">
            <label for="person">{{__('Person')}}</label>
            <input name="person" id="person" class="form-control form-control-rounded" type="text" value="">
            @if (isset($errors) && $errors->has('person'))
                <span class="text-danger"><strong>{{ $errors->first('person') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="comment">{{__('Comment')}}</label>
            <textarea name="comment" id="comment" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('comment'))
                <span class="text-danger"><strong>{{ $errors->first('comment') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('assets/js/dropify.min.js') }}'></script>
    <script>
        $(document).ready(function () {
            $('.dropify').dropify();
        });
    </script>
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('.description-edit-button').on('click', function () {
                $('.hidden').hide();
                $('#edit-description-form').show('slow');
            });
            $('#activity-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-activity-form').show('slow');
            });
            $('.activity-edit-button').on('click', function () {
                $('.hidden').hide();
                $('#edit-activity-form').show('slow');
                let info = $(this).data('info');
                $('#edit-activity-form').find('#id').val(info['id']);
                $('#edit-activity-form').find('#title').val(info['title']);
                $('#edit-activity-form').find('#description').html(info['description']);
            });
            $('#featured-project-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-featured-project-form').show('slow');
            });
            $('.featured-project-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-featured-project-form').show('slow');
                $('#edit-featured-project-form').find('#id').val(info['id']);
                $('#edit-featured-project-form').find('#project_type').val(info['project_type']);
                $('#edit-featured-project-form').find('#title').val(info['title']);
                $('#edit-featured-project-form').find('#subtitle').val(info['subtitle']);
                $('#edit-featured-project-form').find('#description').html(info['description']);
                $('#edit-featured-project-form').find('#url').val(info['url']);
            });
            $('.project-comment-add-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#add-project-comment-form').show('slow');
                $('#add-project-comment-form').find('#user_featured_project_id').val(info['id']);
            });
            $('.project-comment-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-project-comment-form').show('slow');
                $('#edit-project-comment-form').find('#id').val(info['id']);
                $('#edit-project-comment-form').find('#user_featured_project_id').val(info['user_featured_project_id']);
                $('#edit-project-comment-form').find('#person').val(info['person']);
                $('#edit-project-comment-form').find('#comment').html(info['comment']);
            });
        });
    </script>
@endsection
