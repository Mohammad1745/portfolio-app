@extends('auth.layouts.master')
@section('title', __('Sign Up'))
@section('content')
    <div class="mh-home image-bg home-2-img" id="mh-home">
        <div class="img-foverlay img-color-overlay">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">
                    <div class="col-sm-6">
                        <h1 class="mb-3 text-18">{{__('Sign Up')}}</h1>
                        {{ Form::open(['route' => 'signUpProcess', 'method' => 'post']) }}
                            <div class="form-group">
                                <label for="first_name">{{__('First Name')}}</label>
                                <input name="first_name" id="first_name" class="form-control form-control-rounded" type="text" value="{{old('first_name')}}">
                                @if (isset($errors) && $errors->has('first_name'))
                                    <span class="text-danger"><strong>{{ $errors->first('first_name') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="last_name">{{__('Last Name')}}</label>
                                <input name="last_name" id="last_name" class="form-control form-control-rounded" type="text" value="{{old('last_name')}}">
                                @if (isset($errors) && $errors->has('last_name'))
                                    <span class="text-danger"><strong>{{ $errors->first('last_name') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="username">{{__('Username')}}</label>
                                <input name="username" id="username" class="form-control form-control-rounded" type="text" value="{{old('username')}}">
                                @if (isset($errors) && $errors->has('username'))
                                    <span class="text-danger"><strong>{{ $errors->first('username') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                <input name="email" id="email" class="form-control form-control-rounded" type="email" value="{{old('email')}}">
                                @if (isset($errors) && $errors->has('email'))
                                    <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password">{{__('Password')}}</label>
                                <input name="password" id="password" class="form-control form-control-rounded" type="password">
                                @if (isset($errors) && $errors->has('password'))
                                    <span class="text-danger"><strong>{{ $errors->first('password') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">{{__('Confirm Password')}}</label>
                                <input name="password_confirmation" id="password_confirmation" class="form-control form-control-rounded" type="password">
                                @if (isset($errors) && $errors->has('password_confirmation'))
                                    <span class="text-danger"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                @endif
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">{{__('Sign Up')}}</button>
                        {{ Form::close() }}
                        <div class="mt-3 text-center">
                            Already have an account? <a href="{{ route('signIn') }}" class="text-muted"><u>{{__('Sign In')}}</u></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
