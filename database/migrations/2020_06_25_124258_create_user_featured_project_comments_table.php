<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFeaturedProjectCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_featured_project_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_featured_project_id');
            $table->string('person');
            $table->text('comment');
            $table->timestamps();
            $table->foreign('user_featured_project_id')->references('id')->on('user_featured_projects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_featured_project_comments');
    }
}
