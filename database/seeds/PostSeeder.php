<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        Post::create([
            'user_id' => $userId,
            'title' => 'JARVIS',
            'site' => 'Youtube',
            'url' => 'https://www.youtube.com',
            'content' => 'JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy. JARVIS is  an AI made for fantasy.',
            'image' => '5ef87d681f407.png'
        ]);
        Post::create([
            'user_id' => $userId,
            'title' => 'FRIDAY',
            'site' => 'Youtube',
            'url' => 'https://www.youtube.com',
            'content' => 'FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy. FRIDAY is  an AI made for fantasy.',
            'image' => '5ef87d95260fe.jpeg'
        ]);
    }
}
