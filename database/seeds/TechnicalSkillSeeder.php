<?php

use App\Models\TechnicalSkill;
use Illuminate\Database\Seeder;

class TechnicalSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        TechnicalSkill::create([
            'user_id' => $userId,
            'title' => 'AI',
            'percentage' => '95',
        ]);
        TechnicalSkill::create([
            'user_id' => $userId,
            'title' => 'Hardware',
            'percentage' => '96',
        ]);
        TechnicalSkill::create([
            'user_id' => $userId,
            'title' => 'Weapon Tech',
            'percentage' => '91',
        ]);
    }
}
