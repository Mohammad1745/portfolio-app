<?php

use App\Models\PortfolioContent;
use Illuminate\Database\Seeder;

class PortfolioContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $portfolioCategoryId = \App\Models\PortfolioCategory::where('title', 'AI')->first()->id;
        PortfolioContent::create([
            'portfolio_category_id' => $portfolioCategoryId,
            'title' => 'FRIDAY',
            'subtitle' => 'Personal Assistant',
            'url' => 'https://www.youtube.com',
            'image' => '5ef879d71801a.jpeg',
        ]);
        PortfolioContent::create([
            'portfolio_category_id' => $portfolioCategoryId,
            'title' => 'JARVIS',
            'subtitle' => 'Personal Assistant',
            'url' => 'https://www.youtube.com',
            'image' => '5ef87a7f6b387.jpeg',
        ]);
        $portfolioCategoryId = \App\Models\PortfolioCategory::where('title', 'Weapon')->first()->id;
        PortfolioContent::create([
            'portfolio_category_id' => $portfolioCategoryId,
            'title' => 'Missile',
            'subtitle' => 'Weapon',
            'url' => 'https://www.youtube.com',
            'image' => '5ef87a99b3be8.jpg',
        ]);
    }
}
