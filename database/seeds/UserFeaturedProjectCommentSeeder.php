<?php

use App\Models\UserFeaturedProjectComment;
use Illuminate\Database\Seeder;

class UserFeaturedProjectCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userFeaturedProjectId = \App\Models\UserFeaturedProject::where('title', 'JARVIS')->first()->id;
        UserFeaturedProjectComment::create([
            'user_featured_project_id' => $userFeaturedProjectId,
            'person' => 'Farhat Shahir',
            'comment' => 'Good Boy'
        ]);
        UserFeaturedProjectComment::create([
            'user_featured_project_id' => $userFeaturedProjectId,
            'person' => 'Ahsanul Haque',
            'comment' => 'Khela hoppe'
        ]);
    }
}
