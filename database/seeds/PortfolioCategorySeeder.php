<?php

use App\Models\PortfolioCategory;
use Illuminate\Database\Seeder;

class PortfolioCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        PortfolioCategory::create([
            'user_id' => $userId,
            'title' => 'AI',
        ]);
        PortfolioCategory::create([
            'user_id' => $userId,
            'title' => 'Weapon',
        ]);
    }
}
