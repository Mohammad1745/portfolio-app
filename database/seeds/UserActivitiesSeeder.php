<?php

use App\Models\UserActivity;
use Illuminate\Database\Seeder;

class UserActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        UserActivity::create([
            'user_id' => $userId,
            'logo' => 'logo',
            'title' => 'Weapon Production',
            'description' => 'My company produces weapon for US military. My company produces weapon for US military. My company produces weapon for US military. My company produces weapon for US military.',
        ]);
        UserActivity::create([
            'user_id' => $userId,
            'logo' => 'logo Work',
            'title' => 'JARVIS',
            'description' => 'JARVIS is an AI which is made for fantasy. My personal assistant. JARVIS is an AI which is made for fantasy. My personal assistant. JARVIS is an AI which is made for fantasy. My personal assistant.',
        ]);
    }
}
