<?php

use App\Models\UserHeaderMenu;
use Illuminate\Database\Seeder;

class UserHeaderMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '1']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '2']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '3']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '4']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '5']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '6']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '7']);
        UserHeaderMenu::create(['user_id' => $userId, 'header_menu_id' => '8']);
    }
}
