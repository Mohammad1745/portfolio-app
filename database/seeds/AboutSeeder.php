<?php

use App\Models\About;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        About::create([
            'user_id' => $userId,
            'description' => 'I am industrialist, genius inventor, hero and former playboy who is CEO of Stark Industries. This was chief weapons manufacturer for the U.S. military, until I changed of heart and redirects my technical knowledge into the creation of mechanized suits of armor which is used to defend against those that would threaten peace around the world.',
        ]);
    }
}
