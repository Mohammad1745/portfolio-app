<?php

use App\Models\Education;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        Education::create([
            'user_id' => $userId,
            'subject' => 'Physics',
            'institution' => 'MIT',
            'url' => 'https://www.youtube.com',
            'description' => 'Description and description and description. Description and description and description. Description and description and description. Description and description and description. Description and description and description. Description and description and description.',
            'started_at' => '2020-04-01',
            'ended_at' => '2020-06-17'
        ]);
        Education::create([
            'user_id' => $userId,
            'subject' => 'Engineering',
            'institution' => 'MIT',
            'url' => 'https://www.youtube.com',
            'description' => 'Description and description and description. Description and description and description. Description and description and description. Description and description and description. Description and description and description. Description and description and description.',
            'started_at' => '2020-06-02',
            'ended_at' => '2020-06-25'
        ]);
    }
}
